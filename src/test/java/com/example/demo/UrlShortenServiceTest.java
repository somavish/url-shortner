package com.example.demo;

import com.example.demo.repository.UrlShortnerRepository;
import com.example.demo.service.UrlShortnerService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UrlShortenServiceTest {

    @Autowired
    UrlShortnerRepository repo;

    @Autowired
    UrlShortnerService service;

    @Test
    public void shorten_valid_url(){
        String url = "http://myurlshortener.com";
        String shortenUrl = service.getShortenUrl(url, "http://msu/");
        System.out.println(shortenUrl);
    }

    @Test
    public void shorten_invalid_irl(){
        String url = "http:// myurlshortener.com"; //space is present between the http:// and rest of the url
        service.getShortenUrl(url, "http://msu/");
    }    
}
