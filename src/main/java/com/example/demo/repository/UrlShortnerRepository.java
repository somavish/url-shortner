package com.example.demo.repository;

import com.example.demo.entity.UrlShortnerEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UrlShortnerRepository extends JpaRepository<UrlShortnerEntity, Long> {

    @Query("SELECT * from url_info WHERE url=:longUrl")
    public UrlShortnerEntity getUrlEntity(@Param("longUrl") String longUrl);
    
}
