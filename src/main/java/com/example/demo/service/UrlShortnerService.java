package com.example.demo.service;

import org.springframework.stereotype.Service;

@Service
public interface UrlShortnerService {

    String getShortenUrl(String longUrl, String domainName);

    String getKey(String longUrl);

}
