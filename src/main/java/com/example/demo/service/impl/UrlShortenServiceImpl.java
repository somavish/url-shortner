package com.example.demo.service.impl;

import java.util.Objects;
import com.example.demo.entity.UrlShortnerEntity;
import com.example.demo.repository.UrlShortnerRepository;
import com.example.demo.service.UrlShortnerService;
import com.example.demo.util.UrlShortnerUtilService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

public class UrlShortenServiceImpl implements UrlShortnerService{

    @Autowired
    private UrlShortnerRepository repo;

    private static int keyLength = 8;

    private static String EMPTY = "";

    @Override
    public String getShortenUrl(String longUrl, String domainName) {
        String shortURL = "";
        String existingKey = getKey(longUrl);
        if(StringUtils.isEmpty(existingKey)){
		    if (UrlShortnerUtilService.validateUrl(longUrl)) {
                String key = UrlShortnerUtilService.generateKey(keyLength);
			    longUrl = UrlShortnerUtilService.checkUrl(longUrl);
                shortURL = domainName + "/" + key;
                UrlShortnerEntity entity = new UrlShortnerEntity();
                entity.setUrl(longUrl);
                entity.setKey(key);
                repo.save(entity);
            }
		}else{
            shortURL = domainName + "/" + existingKey;
        }
		return shortURL;
    }

	@Override
	public String getKey(String longUrl) {
        UrlShortnerEntity urlEntity = repo.getUrlEntity(longUrl);
        if(Objects.nonNull(urlEntity)) {
            return urlEntity.getKey(); 
        }
        return EMPTY;
	}
    
}
