package com.example.demo.util;

import java.net.URL;
import java.util.Random;

import org.hibernate.validator.internal.constraintvalidators.hv.URLValidator;


public class UrlShortnerUtilService {


    private static char urlChars[];

    UrlShortnerUtilService(){
        urlChars = new char[62];
        for (int i = 0; i < 62; i++) {
			int j = 0;
			if (i < 10) {
				j = i + 48;
			} else if (i > 9 && i <= 35) {
				j = i + 55;
			} else {
				j = i + 61;
			}
			urlChars[i] = (char) j;
		}
    }

    public static String generateKey(int keyLength){
        String key = "";
        Random random = new Random();
		boolean flag = true;
		while (flag) {
			key = "";
			for (int i = 0; i <= keyLength; i++) {
				key += urlChars[random.nextInt(62)];
			}
		}
		return key;
    }

    public static String checkUrl(String url) {
		if (url.substring(0, 7).equals("http://"))
			url = url.substring(7);

		if (url.substring(0, 8).equals("https://"))
			url = url.substring(8);

		if (url.charAt(url.length() - 1) == '/')
			url = url.substring(0, url.length() - 1);
		return url;
    }
    
    public static boolean validateUrl(String url){
        try { 
            new URL(url).toURI(); 
            return true; 
        } 
        catch (Exception e) {
            e.printStackTrace();
            return false; 
        }
    }
    
}
