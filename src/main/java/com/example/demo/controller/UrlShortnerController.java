package com.example.demo.controller;

import com.example.demo.service.UrlShortnerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "/url/shorten/")
public class UrlShortnerController {

    @Autowired
    private UrlShortnerService urlService;

    private static String DOMAIN_NAME = "http://msu/";


    @PostMapping
    public String getShortenUrl(@RequestParam String url){
        return urlService.getShortenUrl(url, DOMAIN_NAME);
    }
    
}
